#include <stdio.h>
int main() {
    int n, x = 0, y;
    printf("Enter an integer: ");
    scanf("%d", &n);
    while (n != 0)
        {
        y = n % 10;
        x = x * 10 + y;
        n = n/10;
       }
    printf("Reversed number = %d", x);
    return 0;
}
