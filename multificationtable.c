#include <stdio.h>
int main()
{
    int num,ans,c=1;
    printf("Enter Positive Integer = ");
    scanf("%d", &num);
    while (ans!= num*num)
    {

        printf("%d * %d = %d \n", num, c, num * c);
        ans=num*c;
        c=c+1;

    }

    return 0;
}

/*1 2 3 4 5
2 4 6 8 10
3 6 9 12 15
4
5*/
